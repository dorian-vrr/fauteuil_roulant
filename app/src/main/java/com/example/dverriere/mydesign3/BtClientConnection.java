package com.example.dverriere.mydesign3;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.util.Log;

import java.io.IOException;
import java.io.OutputStream;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class BtClientConnection extends Thread {

    private static final String LOG_TAG = "BtClientConn";

    private final BluetoothSocket mSocket;
    private final BluetoothDevice mDevice;

    private final OutputStream mOutput;

    public enum State{CONNECT, CONNECTED, DISCONNECT};
    public State mState;

    public BtClientConnection(BluetoothDevice device) {
        //Log.i(LOG_TAG, "progress visible ");
        //Connection.progressBarBT.setVisibility(View.VISIBLE);
        //Connection.lblResult.setText("Connexion à "+ device.getName());
        //Connection.lblResult.setTextColor(Color.BLACK);
        mDevice = device;
        BluetoothSocket socket = null;
        OutputStream out = null;

        try {
            socket = mDevice.createRfcommSocketToServiceRecord(UUID.fromString("00001101-0000-1000-8000-00805F9B34FB"));
            out = socket.getOutputStream();
        } catch (Exception e) {
            Log.e(LOG_TAG, "error while creating BT socket ");
            e.printStackTrace();
        }

        mSocket = socket;
        mOutput = out;

        mState = State.CONNECT;
        //Log.e(LOG_TAG, "progress gone ");
        //Connection.progressBarBT.setVisibility(View.GONE);
    }

    public void run() {
        Log.e(LOG_TAG, "Client start");
        if (mSocket == null) return;

        while (true) {
            switch (mState) {
                case CONNECT:
                    try {
                        mSocket.connect();
                    } catch (IOException e) {
                        e.printStackTrace();
                        mState = State.DISCONNECT;
                        return;
                    }
                    mState = State.CONNECTED;
                    Log.d(LOG_TAG, "Connected!");
                    break;
                case CONNECTED:

                    break;
                case DISCONNECT:
                    if (mSocket != null) {
                        try {
                            mOutput.write('E');
                            TimeUnit.MILLISECONDS.sleep(100);
                            mSocket.close();
                            Log.d(LOG_TAG, "Disconnected");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    return;
            }
        }
    }

    public void send(byte sendData[]) {
        if (!mState.equals(State.CONNECTED)) return;
        try {
            mOutput.write(sendData);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void disconnect() {
        Log.i(LOG_TAG, "BT device disconected as requested by the user");
        mState  = State.DISCONNECT;
    }

}