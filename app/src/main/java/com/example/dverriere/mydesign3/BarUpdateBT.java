package com.example.dverriere.mydesign3;

import android.os.CountDownTimer;

public class BarUpdateBT extends CountDownTimer {

    public BarUpdateBT(long millisInFuture, long countDownInterval) {
        super(millisInFuture, countDownInterval);
    }

    @Override
    public void onTick(long millisUntilFinished) {
        Config.btnSearch.setClickable(false);
        Config.btnSearch.setEnabled(false);
        int progress = 110 - (int) (millisUntilFinished/100);
        Config.progressBarBT.setProgress(progress);
    }

    @Override
    public void onFinish() {
        Config.progressBarBT.setProgress(0);
        Config.btnSearch.setEnabled(true);
        Config.btnSearch.setClickable(true);
    }
}