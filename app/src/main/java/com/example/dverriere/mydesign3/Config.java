package com.example.dverriere.mydesign3;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.net.NetworkInfo;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;

import java.io.OutputStream;
import java.util.ArrayList;

public class Config extends  Fragment{
    // CONSTANTES
    private static final String LOG_TAG = "Bluetooth : Connection";
    private static final int REQUEST_ENABLE_BT = 1;

    // ADAPTERS
    private BluetoothAdapter mBluetoothAdapter;
    private BluetoothSocket mBluetoothSocket;
    private OutputStream mOutBT;
    private NetworkInfo.State mStateBT;
    private BluetoothDevice mDeviceBT;
    ArrayList<BluetoothDevice> BTdevices = new ArrayList();


    protected static BtClientConnection mConnection;

    // WIDGETS from UI file
    Switch switchBT;
    public static Button btnSearch;
    LinearLayout lytBT;
    public static ProgressBar progressBarBT;
    public TextView lblResult;

    BarUpdateBT taskPBBT = new BarUpdateBT(12000,50);

    int nbBT = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.i(LOG_TAG, "Config::onCreateView");
        View rootView = inflater.inflate(R.layout.config, container, false);
        /*
        switchBT = (Switch) rootView.findViewById(R.id.switchBT);
        switchBT.setOnClickListener(mSwitchBTListener);
*/
        Toolbar toolbar = (Toolbar) rootView.findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);

        Log.i(LOG_TAG, "Connection::onCreate");

        switchBT = (Switch) rootView.findViewById(R.id.switchBT);
        switchBT.setOnClickListener(mSwitchBTListener);

        btnSearch = (Button) rootView.findViewById(R.id.btnSearch);
        btnSearch.setOnClickListener(mSearchListener);

        lytBT = (LinearLayout) rootView.findViewById(R.id.lytBT);

        progressBarBT = (ProgressBar) rootView.findViewById(R.id.progressBarBT);
        progressBarBT.setProgress(0);


        //taskPBBT.start(); just test

        lblResult = (TextView) rootView.findViewById(R.id.lblResult);
        lblResult.setText("STA");
        //lblResult.setTextColor(Color.BLACK);

        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null) {
            // Device doesn't support Bluetooth
            Log.e(LOG_TAG, "Device doesn't support Bluetooth");
            switchBT.setChecked(false);
            btnSearch.setEnabled(false);
        } else {
            switchBT.setChecked(mBluetoothAdapter.isEnabled());
            btnSearch.setEnabled(true);
        }

        // Register for broadcasts when a device is discovered.
        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        //registerReceiver(mReceiver, filter);
        getActivity().registerReceiver(mReceiver,filter);

        return rootView;
    }
    // changestate of the switchBtnBT
    private View.OnClickListener mSwitchBTListener = new View.OnClickListener() {
        public void onClick(View v) {
            if(switchBT.isChecked()) {
                Log.i(LOG_TAG, "switchBT checked -> try to set up BT");
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
                btnSearch.setEnabled(true);
            } else {
                for (View btn : lytBT.getTouchables())
                    lytBT.removeView(btn);
                Log.i(LOG_TAG, "switchBT unchecked -> switch off BT");
                mBluetoothAdapter.disable();
                btnSearch.setEnabled(false);
            }
        }
    };

    // OnClickListener of the btnSearch
    private View.OnClickListener mSearchListener = new View.OnClickListener() {
        public void onClick(View v) {
            nbBT = 0;
            BTdevices.clear();
            for (View btn : lytBT.getTouchables())
                lytBT.removeView(btn);
            if(switchBT.isChecked()) {
                Log.i(LOG_TAG, "Click on search with enable BT");
                taskPBBT.start();
                /*Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();

                if (pairedDevices.size() > 0) {
                    // There are paired devices. Get the name and address of each paired device.
                    for (BluetoothDevice device : pairedDevices) {
                        String deviceName = device.getName();
                        String deviceHardwareAddress = device.getAddress(); // MAC address
                        Log.i(LOG_TAG, "PAIRED :\tdeviceName = " + deviceName + "\tdeviceHardwareAddress = " + deviceHardwareAddress);
                    }
                }*/
                mBluetoothAdapter.startDiscovery();
            } else {
                Log.e(LOG_TAG, "Click on search with BT disabled ?");
            }
        }
    };

    // Create a BroadcastReceiver for ACTION_FOUND.
    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                // Discovery has found a device. Get the BluetoothDevice
                // object and its info from the Intent.
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                String deviceName = device.getName();
                String deviceHardwareAddress = device.getAddress(); // MAC address
                Log.i(LOG_TAG, "DISCOVER :\tdeviceName = " + deviceName + "\tdeviceHardwareAddress = " + deviceHardwareAddress);
                BTdevices.add(device);
                Button btnTag = new Button(context);
                btnTag.setText(deviceName + " - " + deviceHardwareAddress);
                btnTag.setId(nbBT);
                lytBT.addView(btnTag);
                ((Button) getView().findViewById(nbBT)).setOnClickListener(deviceClickListener);
                nbBT ++;
            }
        }
    };

    //click on a device to connect with
    private View.OnClickListener deviceClickListener = new View.OnClickListener() {
        public void onClick(final View v) {
            btnSearch.setEnabled(false);
            if(switchBT.isChecked()) {
                Log.i(LOG_TAG, "Click on BT device, id = "+v.getId() + " deviceName = " + BTdevices.get(v.getId()).getName() + "\tdeviceHardwareAddress = " + BTdevices.get(v.getId()).getAddress());
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);

                lblResult.setText("Connexion à "/*+ BTdevices.get(v.getId()).getName()+" ..."*/);
                lblResult.setTextColor(Color.BLACK);

                //mConnection = new BtClientConnection(BTdevices.get(v.getId()));
                //mConnection = new BtClientConnection(BTdevices.get(v.getId()).getAddress());
                mConnection = new BtClientConnection(BTdevices.get(v.getId()));
                mConnection.start();
                Log.e(LOG_TAG, "Connexion à "+ BTdevices.get(v.getId()).getName()+" ...");
                //progressBarBT.setVisibility(View.VISIBLE);
                //progressBarBT.setAlpha(1f);
                while(mConnection.mState == BtClientConnection.State.CONNECT){
                    SystemClock.sleep(300);
                    //Log.e(LOG_TAG, "SLEEP");
                }
                Log.e(LOG_TAG, mConnection.mState.name());
                if(mConnection.mState==BtClientConnection.State.CONNECTED /*|| true */){
                    //Intent myIntentConnected = new Intent(getBaseContext(),Connected.class); //getBaseContext ?
                    //startActivity(myIntentConnected);
                    lblResult.setText("Connexion à "+ BTdevices.get(v.getId()).getName()+" réussie");
                }

                lblResult.setText("Connexion à "+ BTdevices.get(v.getId()).getName()+" impossible");
                lblResult.setTextColor(Color.RED);
            } else {
                Log.e(LOG_TAG, "Click on BT device with BT disable ?!");
            }
            btnSearch.setEnabled(true);
        }
    };



    @Override
    public void onDestroy() {
        super.onDestroy();

        // Don't forget to unregister the ACTION_FOUND receiver
        //getVunregisterReceiver(mReceiver);
    }


}
